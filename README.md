# Task

Task:
1. Display cars on map.
2. Display cars in a list. Don't forget to show images.
3. Create filter and sort for cars:
	3.1. Filter by Plate number
	3.2. Filter by remaining battery
	3.3. Sort by distance from user
4. Add easy way to switch between Map/List/Filter views.

Bonus:
5. UI/Unit tests
6. MVVM
7. Rx
