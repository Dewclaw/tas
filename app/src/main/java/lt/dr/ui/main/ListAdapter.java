package lt.dr.ui.main;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lt.dr.R;
import lt.dr.entity.AvailableCars;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> implements Filterable {
    private static final String TAG = "ListAdapter";

    private List<AvailableCars> carsList;
    private List<AvailableCars> carsListForFull;



    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView carTitle;
        TextView isCharging;
        TextView batteryPercentage;
        TextView batteryEstimatedDistance;
        TextView plateNumber;
        ViewHolder(@NonNull View v) {
            super(v);
            imageView = v.findViewById(R.id.img);
            carTitle = v.findViewById(R.id.carTitle);
            isCharging = v.findViewById(R.id.isCharging);
            batteryEstimatedDistance = v.findViewById(R.id.batteryEstimatedDistance);
            batteryPercentage = v.findViewById(R.id.batteryPercentage);
            plateNumber = v.findViewById(R.id.plate_number);
        }
    }

    ListAdapter(List<AvailableCars> list){
        carsList = list;
        carsListForFull = new ArrayList<>(list);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AvailableCars availableCars = carsList.get(position);
        holder.carTitle.setText(availableCars.getModel().getTitle());
        holder.batteryPercentage.setText(String.format("batteryPercentage: %s", availableCars.getBatteryPercentage()));
        holder.batteryEstimatedDistance.setText(String.format("batteryEstimatedDistance: %s", availableCars.getBatteryEstimatedDistance()));
        holder.isCharging.setText(String.format("isCharging: %s", availableCars.isCharging()));
        holder.plateNumber.setText(String.format("plateNumber: %s", availableCars.getPlateNumber()));
        Picasso.get()
                .load(availableCars.getModel().getPhotoUrl())
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return carsList.size();
    }

    Filter getFilterPlateNumbers() {
        return filterPlateNumbers;
    }

    Filter getFilterRemainingBattery() {
        return filterRemainingBattery;
    }
    @Override
    public Filter getFilter() {
        return null;
    }
    private final Filter filterPlateNumbers = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence sequence) {
            List<AvailableCars> filteredList = new ArrayList<>();
            if (sequence == null || sequence.length() == 0) {
                filteredList.addAll(carsListForFull);
            } else {
                String filterPattern = sequence.toString().toLowerCase().trim();
                for (AvailableCars cars : carsListForFull) {
                    Log.d(TAG, "performFiltering: Pattern: " + filterPattern);
                    Log.d(TAG, "performFiltering: checking:" + cars.getPlateNumber());
                    if (cars.getPlateNumber().toLowerCase().contains(filterPattern))
                        filteredList.add(cars);
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            carsList.clear();
            Log.d(TAG, "publishResults: " + filterResults.count);
            carsList.addAll((List) filterResults.values);
            notifyDataSetChanged();
        }
    };
    private final Filter filterRemainingBattery = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence sequence) {
            List<AvailableCars> filteredList = new ArrayList<>();
            if (sequence == null || sequence.length() == 0) {
                filteredList.addAll(carsListForFull);
            } else {
                String filterPattern = sequence.toString().trim();
                for (AvailableCars cars : carsListForFull) {
                    Log.d(TAG, "performFiltering: Pattern: " + filterPattern);
                    Log.d(TAG, "performFiltering: checking:" + cars.getBatteryPercentage());
                    if (String.valueOf(cars.getBatteryPercentage()).contains(sequence))
                        filteredList.add(cars);
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            carsList.clear();
            Log.d(TAG, "publishResults: " + filterResults.count);
            carsList.addAll((List) filterResults.values);
            notifyDataSetChanged();
        }
    };
    void sort(boolean b){
        MapFragment mapFragment = MapFragment.getInstance();
        Map<AvailableCars,Float> map = mapFragment.getHashMap();
        List<AvailableCars> temto = new ArrayList<>();
        Log.d(TAG, "sort: Before");
        Log.d(TAG, "sort: carListSize: " + carsList.size());
        Log.d(TAG, "sort: tempoSize: " +temto.size());
        if (b) {
            Log.d(TAG, "sort: True");
            temto.clear();
            map.entrySet()
                    .stream()
                    .sorted((t1, t2) -> t1.getValue().compareTo(t2.getValue()))
                    .forEach((v) -> carsList.forEach(cars -> {
                        if (v.getKey().getId() == cars.getId())
                            temto.add(cars);
                    }));
        }else {
            Log.d(TAG, "sort: False");
            temto.clear();
            map.entrySet()
                .stream()
                .sorted((t1, t2) -> t2.getValue().compareTo(t1.getValue()))
                .forEach((v) -> carsList.forEach(cars -> {
                    if (v.getKey().getId() == cars.getId())
                        temto.add(cars);
                }));
        }
        carsList.clear();
        carsList = temto;
        Log.d(TAG, "sort: After");
        Log.d(TAG, "sort: carListSize: " + carsList.size());
        Log.d(TAG, "sort: tempoSize: " +temto.size());
        notifyDataSetChanged();
    }

}
