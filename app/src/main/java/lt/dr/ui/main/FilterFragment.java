package lt.dr.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Switch;

import androidx.fragment.app.Fragment;

import lt.dr.R;


public class FilterFragment extends Fragment {
    private static final String TAG = "FilterFragment";
    private ListAdapter adapter;
    private Switch aSwitch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        SearchView plateNumber = view.findViewById(R.id.searchPlateNumber);
        SearchView remainingBattery = view.findViewById(R.id.searchRemainingBattery);
        aSwitch = view.findViewById(R.id.switch2);
        aSwitch.setOnClickListener(e->{
            adapter.sort(aSwitch.isChecked());
        });
        ListFragment listFragment = ListFragment.getInstance();
        adapter = listFragment.getAdapter();
        plateNumber.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilterPlateNumbers().filter(s);
                return false;
            }
        });
        remainingBattery.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilterRemainingBattery().filter(s);
                return false;
            }
        });
        return view;
    }

}
