package lt.dr.ui.main;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import lt.dr.R;
import lt.dr.entity.AvailableCars;
import lt.dr.utils.JsonApi;
import lt.dr.utils.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.LOCATION_SERVICE;

public class MapFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "MapFragment";
    private static List<AvailableCars> carsList;
    private GoogleMap googleMap;
    private LayoutInflater inflater;
    private static MapFragment mapFragment;
    private Location userLoc;
    private Map<AvailableCars,Float> hashMap = new HashMap<>();
    static MapFragment getInstance(){
        return mapFragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        this.inflater = inflater;
        mapFragment = this;
        MapView mapView = view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(Objects.requireNonNull(getActivity()).getApplicationContext());
        }catch (Exception e){
            Log.d(TAG, "onCreateView: MapInit " + e.getMessage());
            e.printStackTrace();
        }
        mapView.getMapAsync(this);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getData();
    }
    /*
    * Getting data from api
    * Calculating distance
    * adding makers
    * */
    private void getData() {
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance(getContext().getString(R.string.ApiCallBase));
        JsonApi jsonApi = retrofit.create(JsonApi.class);
        Call<List<AvailableCars>> call = jsonApi.getAvailableCars();
        call.enqueue(new Callback<List<AvailableCars>>() {
            @Override
            public void onResponse(Call<List<AvailableCars>> call, Response<List<AvailableCars>> response) {
                if (!response.isSuccessful()){
                    Log.d(TAG, "onResponse: Code " + response.code());
                }
                hashMap.clear();
                Location tempLoc = new Location("");
                carsList = response.body();

                for(AvailableCars availableCars : carsList){
                    //temp location
                    tempLoc.setLatitude(availableCars.getLocation().getLatitude());
                    tempLoc.setLongitude(availableCars.getLocation().getLongitude());
                    //Distance to car
                    Log.d(TAG, String.format("onResponse: Distance from user to: %s plateNumber %s is: %s meters or %s km",
                            availableCars.getModel().getTitle(),
                            availableCars.getPlateNumber(),
                            userLoc.distanceTo(tempLoc),
                            userLoc.distanceTo(tempLoc)/1000));
                    hashMap.put(availableCars,userLoc.distanceTo(tempLoc));
                    //Tried add custom icon to maker
                  /*  Picasso.get().load(availableCars.getModel().getPhotoUrl()).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bit, Picasso.LoadedFrom from) {
                            bitmap = bit;
                            Log.d(TAG, "onBitmapLoaded: Bitmap");
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
                    Log.d(TAG, "onResponse: Too late");*/
                    //**********
                    googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(
                                    availableCars.getLocation().getLatitude(),
                                    availableCars.getLocation().getLongitude()))
                            .title(availableCars.getModel().getTitle() + "\n" +
                                    availableCars.getPlateNumber()));
                }
//                Log.d(TAG, "onResponse: " + availableCars);
            }

            @Override
            public void onFailure(Call<List<AvailableCars>> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(54.71978000, 25.28478000))
                .zoom(12)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        Log.d(TAG, "onMapReady: MapReady");
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
            Criteria criteria = new Criteria();
            LocationManager locationManager = (LocationManager) inflater.getContext().getSystemService(LOCATION_SERVICE);
            String provider = locationManager.getBestProvider(criteria, true);
            userLoc = locationManager.getLastKnownLocation(provider);
        } else {
            Log.d(TAG, "onCreateView: No permission");
            // Show rationale and request permission.
        }
    }

    Map<AvailableCars, Float> getHashMap() {
        return hashMap;
    }
}
