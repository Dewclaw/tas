package lt.dr.ui.main;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import lt.dr.R;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {
    private static final String TAG = "SectionsPagerAdapter";
    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                Log.d(TAG, "getItem: newMapFrag");
                fragment = new MapFragment();
                break;
            case 1:
                Log.d(TAG, "getItem: newListFrag");
                fragment = new ListFragment();
                break;
            case 2:
                Log.d(TAG, "getItem: newFilterFrag");
                fragment = new FilterFragment();
                break;
        }
        return fragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return mContext.getString(R.string.Tab1);
            case 1:
                return mContext.getString(R.string.Tab2);
            case 2:
                return mContext.getString(R.string.Tab3);
        }
        return null;
    }




    @Override
    public int getCount() {
        // Show 2 total pages.
        return 3;
    }
}