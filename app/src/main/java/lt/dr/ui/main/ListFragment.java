package lt.dr.ui.main;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import lt.dr.R;
import lt.dr.entity.AvailableCars;
import lt.dr.utils.JsonApi;
import lt.dr.utils.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class ListFragment extends Fragment {
    private static final String TAG = "ListFragment";
    private RecyclerView recyclerView;
    private ListAdapter adapter;
    private List<AvailableCars> carsList;
    private static ListFragment listFragment;

    static ListFragment getInstance(){
        return listFragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        listFragment = this;
        recyclerView = view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
//        if (carsList == null)
//            Log.d(TAG, "onCreateView: carListNull");
        getData();
        return view;
    }

    private void getData() {
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance(getContext().getString(R.string.ApiCallBase));
        JsonApi jsonApi = retrofit.create(JsonApi.class);
        Call<List<AvailableCars>> call = jsonApi.getAvailableCars();
        call.enqueue(new Callback<List<AvailableCars>>() {
            @Override
            public void onResponse(Call<List<AvailableCars>> call, Response<List<AvailableCars>> response) {
                if (!response.isSuccessful()){
                    Log.d(TAG, "onResponse: Code " + response.code());
                }
                carsList = response.body();
                adapter = new ListAdapter(carsList);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<AvailableCars>> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    ListAdapter getAdapter() {
        return adapter;
    }
}
