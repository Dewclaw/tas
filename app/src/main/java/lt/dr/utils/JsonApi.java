package lt.dr.utils;

import java.util.List;

import lt.dr.entity.AvailableCars;
import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonApi {

    @GET("api/mobile/public/availablecars")
    Call<List<AvailableCars>> getAvailableCars();
}
